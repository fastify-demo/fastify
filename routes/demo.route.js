const {postDemoOpt,getDemoOpt,putDemoOpt,deleteDemoOpt}=require("../schema/demo.schema")

function demoRoutes(fastify, options, done) {

 
  // Create Document (POST)
  fastify.post('/create',postDemoOpt);


  //Read Document(GET)
  fastify.get('/read',getDemoOpt )


  // Update Document (PUT)
  fastify.put('/update', putDemoOpt);


  // Delete Document (DELETE)
  fastify.delete('/delete', deleteDemoOpt);


  done()
}


module.exports = demoRoutes