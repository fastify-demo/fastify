const{postdata,getdata,putdata,deletedata}=require('../controllers/demoController')




const postDemoOpt ={
  schema: {
    response: {
      200: {
        type: "object",
        properties: {
          _id: { type: "string" }
        },

      }
    },

  },
  handler: postdata
};


const getDemoOpt = {
  schema: {
    response: {
      200: {
        type: "object",
        properties: {
          _index: { type: "string" },
          _id: { type: "string" },
          _source: {
            type: "object",
            properties: {
              title: { type: "string" }
            },

          }
        },

      }
    }
  },
  handler: getdata
};

const putDemoOpt = {
  schema: {
    response: {
      200: {
        type: "object",
        properties: {
          _index: { type: "string" },
          _id: { type: "string" },
          result: { type: "string" },
        },

      }
    }
  },
  handler: putdata

};


const deleteDemoOpt = {
  schema: {
    response: {
      200: {
        type: "object",
        properties: {
          _index: { type: "string" },
          _id: { type: "string" },
          result: { type: "string" },
        },

      }
    }
  },
  handler: deletedata
}


module.exports={postDemoOpt,getDemoOpt,putDemoOpt,deleteDemoOpt}