


  async function postdata(request, reply) {
    const { index, id, body } = request.body;

    try {
      console.log("id", id);
      const response = await this.elastic.index({
        index,
        id,
        body
      });
      reply.send(response)
    } catch (err) {
      // Handle errors
      console.error(err);
      reply.status(500).send({ error: 'Internal Server Error' });
    }
  }


  async function getdata(req, reply) {
    const { id, index } = req.query;
    console.log("id", id, index)
    const response = await this.elastic.get({
      index,
      id
    })

    reply.status(200).send(response)
  }

  async function putdata(req, reply) {
    const { id, index } = req.query;
    const { body } = req.body;

    try {
      const response = await this.elastic.update({

        index,
        id,
        body: {
          doc: body,
        },
      });

      reply.status(200).send(response);
    } catch (error) {
      reply.status(500).send({ error: 'Error updating document' });
    }
  }


  async function deletedata(req, reply) {
    const { index, id } = req.query;

    try {

      const response = await this.elastic.delete({
        index,
        id,
      });

      reply.status(200).send(response);
    } catch (error) {
      reply.status(500).send({ error: 'Error deleting document' });
    }
  }

  module.exports={postdata,getdata,putdata,deletedata}

 


