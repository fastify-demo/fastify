const tap = require('tap');
const Fastify = require('fastify');
const demoRoutes = require('./routes/demo.route');


const mockElastic = {
    index: async ({ index, id, body }) => {
        
        return {  _index: index,  
        _id: id,         
     };
    },
    get: async ({ index, id }) => {
       
        return {  _index: index,  
            _id: id,         
         };
      },
    update: async ({ index, id, body }) => {
        const res="updated"
        const updatedDocument = {
          _index: index,
          _id: id,
          result: res,
        };
        return { body: updatedDocument };
      },
      delete: async ({ index, id }) => {
       
        const deletedDocument = {
          _index: index,
          _id: id,
          result: 'deleted',
        
        };
        return { body: deletedDocument };
      },
};


const fastify = Fastify();


fastify.decorate('elastic', mockElastic);


fastify.register(demoRoutes);//


tap.test('POST /create should create a document', async (t) => {
  try {
    const payload = {
      index: 'tutorial61',
      id: '221',
      body: { title: 'Hello, world!' }
    };

    const response = await fastify.inject({
      method: 'POST',
      url: '/create',
      payload: payload
    });

const responseBody = JSON.parse(response.body);
    
    t.equal(response.statusCode, 200, 'Status code should be 200');
    t.equal(responseBody.id,"221", 'Response should contain ID of the created document');
  } catch (error) {
    t.error(error, 'No error should be thrown');
  }
});






tap.test('GET /read should retrieve a document', async (t) => {
    try {
     
      const index = 'tutorial6';
      const documentId = '13';
  
      
      const response = await fastify.inject({
        method: 'GET',
        url: `/read?id=${documentId}&index=${index}`
      });
  
      
      t.equal(response.statusCode, 200, 'Status code should be 200');
     
      const responseBody = JSON.parse(response.payload);
      t.equal(responseBody._index, index, 'Retrieved document index should match requested index');
      t.equal(responseBody._id, documentId, 'Retrieved document ID should match requested ID');
     
    } catch (error) {
      
      t.error(error, 'No error should be thrown');
    }
  });
  



  tap.test('PUT /update should update a document', async (t) => {
    try {
      const payload = {
        index: 'tutorial6',
        id: '13',
        body: { message: 'Updated' }
      };
  
      const response = await fastify.inject({
        method: 'PUT',
        url: '/update?id=13&index=tutorial6',
        payload: payload
      });
  
 
     const responseBody = JSON.parse(response.body);
    
      t.equal(response.statusCode, 200, 'Status code should be 200');
      t.equal(responseBody.body.result, 'updated', 'Document should be updated');
    } catch (error) {
      t.error(error, 'No error should be thrown');
    }
  });




tap.test('DELETE /delete should delete a document', async (t) => {
    try {
      const response = await fastify.inject({
        method: 'DELETE',
        url: '/delete?id=21&index=tutorial6',
      });
  
      const responseBody = JSON.parse(response.body);
      t.equal(response.statusCode, 200, 'Status code should be 200');
      t.equal(responseBody.body.result, 'deleted', 'Document should be deleted');
    } catch (error) {
      t.error(error, 'No error should be thrown');
    }
  });