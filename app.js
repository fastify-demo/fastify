
const Fastify = require("fastify");
const fastify = Fastify({
  logger: true,
  requestTimeout: 30000,
})

const { Client } = require('@elastic/elasticsearch');
const client = new Client({ node: 'http://localhost:9200' });


fastify.decorate('elastic', client)


const demoroutes = require("./routes/demo.route")
fastify.register(demoroutes)

const port = 3000;


fastify.listen({ port }, function (err, address) {
  if (err) {
    fastify.log.error(err);
    process.exit(1);
  }

  fastify.log.info(`Fastify is listening on port: ${address}`);
});


module.exports = fastify

